public class Driver {

/*    public void show(){
      Car carSample = new Car();
        carSample.make = "Crosswind";
        System.out.println(carSample.make);
*/
    //Attributes
        private String name;
        private int age;
        private String address;

/*
    Classes have relationships:
    A Car can have a Driver. - "has relationship" - this is called a COMPOSITION which allows modelling objects to be made up of other objects.
*/

        //default constructor

    public Driver() {
    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //Parameterized Constructor
    public Driver (String name, int age, String address){
        this.name = name;
        this.age = age;
        this.address = address;

    }
}
